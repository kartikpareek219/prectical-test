<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Users</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('plugins/DataTables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
</head>

<body>
    <div class="container mt-4">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" style="display: flex; justify-content: space-between;">
                        <h3>{{ __('Users') }}</h3>
                        <button class="btn btn-outline-primary btn-rounded-20 addUserModal">
                            Add User
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-show">
                            <table id="userTable" class="table">
                                <thead>
                                    <tr>
                                        <th>{{ __('##')}}</th>
                                        <th>{{ __('Name')}}</th>
                                        <th>{{ __('Email')}}</th>
                                        <th>{{ __('Phone')}}</th>
                                        <th>{{ __('Description')}}</th>
                                        <th>{{ __('Role')}}</th>
                                        <th>{{ __('Profile Image')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- User data will be dynamically inserted here -->
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- <h3>User Form</h3>

        <button type="button" class="btn btn-primary mb-3 addUserModal">Add User</button> -->

        <!-- User Form Modal -->
        <div class="modal fade" id="addUserModal" aria-hidden="true" data-backdrop="static" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addUserModalLabel">Add User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="userForm" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" id="phone" name="phone">
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" name="description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="role_id">Role</label>
                                <select class="form-control" id="role_id" name="role">
                                    <option value="">None</option>
                                    @foreach ($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="profile_image">Profile Image</label>
                                <input type="file" class="form-control-file" id="profile_image" name="profile_image" accept="image/png, image/png, image/jpeg">
                            </div>
                            <label id="profile_image_err" class="error"></label>
                            <div id="profile_image_display" style="display: none;">
                                <div class="form-group">
                                    <div class="copy_class">
                                        <img id="profile_image_show" width="100px" height="100px" src="" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
    <script src="{{ asset('plugins/DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-validate/jquery.validate-1.19.3.min.js') }}"></script>
    <script src="{{ asset('plugins/notify/notify.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            loadTable();
        });

        $(document).on('click', '.addUserModal', function() {
            //reset form
            $("#userForm")[0].reset();
            //open modal
            $('#addUserModal').modal('show');

            document.getElementById('profile_image_display').style.display = 'none';

            $('label.error').remove();

            // initialize validation function
            validatioForm();
        });

        function validatioForm() {
            //add update item
            $.validator.addMethod(
                "customEmailValidation",
                function(value, element) {
                    // Regular expression pattern for email validation
                    var emailPattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
                    return this.optional(element) || emailPattern.test(value);
                },
                "Please enter a valid email address."
            );
            $("#userForm").validate({
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true,
                        customEmailValidation: true, // Add custom validation rule
                    },
                    phone: {
                        required: true,
                        minlength: 10,
                        maxlength: 10,
                        number: true,
                    },
                    description: {
                        required: true,
                    },
                    role: {
                        required: true,
                    },
                    profile_image: {
                        required: true,
                    }
                },
                errorPlacement: function(error, element) {
                    error.insertAfter(element.parent());
                },
                messages: {
                    email: {
                        email: "Please enter a valid email address.",
                        pattern: "Please enter a valid email address.",
                        customEmailValidation: "Please enter a valid email address.",
                    },
                    profile_image: {
                        extension: "Please upload an image file of type: JPG, PNG, or JPEG.",
                    },
                    phone: {
                        minlength: "Please enter 10 digit phone number.",
                        maxlength: "Please enter 10 digit phone number.",
                    }
                },
                submitHandler: function(form) {
                    var formData = new FormData($("#userForm")[0]);

                    var url_up = "{{url('/')}}" + "/users";
                    $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: url_up,
                        data: formData,
                        mimeType: "multipart/form-data",
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend: function() {
                            $("#userForm").find('.submit_button').attr("disabled", true);
                        },
                        success: function(data) {
                            $("#userForm").find('.submit_button').attr("disabled", false);

                            var response = JSON.parse(data);
                            //console.log(response);
                            if (response.status == true) {
                                //show notification
                                $('#addUserModal').modal('hide');
                                // show success message
                                $.notify(response.message, "success");
                                //in case of edit load active page data
                                var active_page = $(".pagination").find("li.active a").text();
                                $('#userTable').dataTable().fnPageChange(parseInt(active_page) - 1);
                            } else {
                                $.notify(response.message, "warning");
                            }
                        },
                    });
                    return false;

                }
            });

        }


        function loadTable() {
            //listing data table
            $('#userTable').dataTable().fnDestroy();
            var table = $('#userTable').DataTable({
                pageLength: 10,
                lengthMenu: [
                    [10, 100, 200, 500],
                    [10, 100, 200, 500]
                ],
                sDom: 'tr<"bottom" <"row" <"col-sm-4" l><"col-sm-3" i><"col-sm-5" p>>>', //'<"top" <"row" <"col-sm-9" l> <"col-sm-3" f> >>tr<"bottom" <"row" <"col-sm-6" i> <"col-sm-6" p>>>',
                processing: true,
                serverSide: true,
                ajax: {
                    url: '/',
                    type: "get",
                    dataType: "json",
                    data: {}, // Pass data to controller by this
                    complete: function(data) {

                    },
                },
                order: [
                    [0, 'desc']
                ],
                columnDefs: [{
                        targets: 0,
                        data: 'id',
                        name: 'id'
                    },
                    {
                        targets: 1,
                        data: 'name',
                        name: 'name'

                    },
                    {
                        targets: 2,
                        data: 'email',
                        name: 'email'
                    },
                    {
                        targets: 3,
                        className: "lg-text-limit",
                        data: 'phone',
                        name: 'phone'
                    },
                    {
                        targets: 4,
                        data: 'description',
                        name: 'description'
                    },
                    {
                        targets: 5,
                        data: 'role_id',
                        name: 'role_id'
                    },
                    {
                        targets: 6,
                        data: 'profile_image',
                        name: 'profile_image',
                        orderable: false,
                    },
                ],
                oLanguage: {
                    "sEmptyTable": "No records available"
                }
            });
        }

        // display image or pdf file after choosen
        function imagesPreview(input, image_name) {

            if (input.files) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    if (!input.files[i].name.match(/\.(jpg|jpeg|png)$/i)) {
                        var fileExtension = ['jpeg', 'jpg', 'png'];
                        document.getElementById(image_name + '_err').innerHTML = 'Only formats are allowed : ' + fileExtension.join(', ');
                        document.getElementById(image_name + '_display').style.display = 'none';
                        $('#' + image_name).val('');
                        continue;
                    } else {
                        document.getElementById(image_name + '_err').innerHTML = '';
                        document.getElementById(image_name + '_display').style.display = 'block';
                        
                        document.getElementById(image_name + '_show').src = window.URL.createObjectURL(input.files[i]);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#profile_image').on('change', function() {

            imagesPreview(this, 'profile_image');

        });
    </script>
</body>

</html>