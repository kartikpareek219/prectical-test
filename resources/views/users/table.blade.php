<table class="table mt-4">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Description</th>
            <th>Role</th>
            <th>Profile Image</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->description }}</td>
                <td>{{ $user->role->name }}</td>
                <td>
                    <img src="{{ asset('images/' . $user->profile_image) }}" alt="Profile Image" style="max-width: 100px;">
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
