<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct(Request $request)
    {
        $this->sortableColumns = ['id','name', 'email', 'phone', 'description', 'role_id'];
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $limit = $request->input('length');
            $start = $request->input('start');
            $search = $request['search']['value'];

            $orderby = $request['order']['0']['column'];
            $order = $orderby != "" ? $request['order']['0']['dir'] : "";
            $draw = $request['draw'];
            $sortableColumns = $this->sortableColumns;

        
            $s_data = $request->all();

            $response = $this->getData($search, $sortableColumns[$orderby], $order, $s_data);
            $totaldata = $response->count();
            $response = $response->offset($start)->limit($limit)->orderBy('id', 'desc')->get();

            if (!$response) {
                $data = [];
            } else {
                $data = $response;
            }
            
            $datas = [];
            $i = 1;
            
            foreach ($data as $value) {
                //echo $value->role->name."<pre>"; print_r($value); die;
                $row['id'] = $i;
                $row['name'] = $value->name;
                $row['email'] =  (!empty($value->email) ? $value->email : '');
                $row['phone'] = (!empty($value->phone) ? $value->phone : '');
                $row['description'] = (!empty($value->description) ? $value->description : '');
                $row['role_id'] = (!empty($value->role->name) ? $value->role->name : '');
                
                if ($value->profile_image) {
                    $row['profile_image'] = '<a href="javascript:"><img src="'.url('/images/' . $value->profile_image).'" alt="" title="" width="50" height="50"></a>';
                } else {
                    $row['profile_image'] = '';
                }
                $datas[] = $row;
                $i++;
            }
            //print_r($datas); die;
            $return = [
                "draw" => intval($draw),
                "recordsFiltered" => intval($totaldata),
                "recordsTotal" => intval($totaldata),
                "data" => $datas,
            ];
            return $return;
        } else {
            $roles = Role::all();
            $data = ['roles' => $roles];
            return view('users.index', $data);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email:filter|unique:users',
            'phone' => 'required|regex:/^\d{10}$/',
            'description' => 'required',
            'role' => 'required|exists:roles,id',
            'profile_image' => 'required|image|mimes:jpeg,png,jpg',
        ]);

        if ($validator->fails()) {
            return response()->json(["status" => false, 'message' => $validator->errors()->first()], 200);
        }

        $profileImage = $request->file('profile_image');
        $profileImageName = time() . '.' . $profileImage->getClientOriginalExtension();
        $profileImage->move(public_path('images'), $profileImageName);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'description' => $request->description,
            'role_id' => $request->role,
            'profile_image' => $profileImageName,
        ]);

        return response()->json(["status" => true, 'message' => 'User created successfully.'], 200);
    }

    public function getData($search = null, $orderby = null, $order = null, $request = null)
    {
        $orderby = $orderby ? $orderby : 'id';
        $order = $order ? $order : 'desc';
        $response = User::with('role')->orderBy($orderby, $order);
        return $response;
    }
}
