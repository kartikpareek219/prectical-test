<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id()->comment('Primary key');
            $table->string('name')->comment('User\'s name');
            $table->string('email')->unique()->comment('User Email');
            $table->string('phone')->comment('User Phone Number');
            $table->text('description')->comment('Description');
            $table->unsignedBigInteger('role_id')->comment('User Role Id');
            $table->string('profile_image')->comment('User Profile Image');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
