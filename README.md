# Prectical Test

1. Clone the Project: If you haven't already, clone the Laravel project from the repository using Git or download and extract the project files to a local directory.

2. Configure Environment: Laravel requires some configuration settings to run correctly. Copy the .env.example file and rename it to .env. Open the .env file and configure the necessary settings such as database connection details.

3. Install Dependencies: Open a terminal or command prompt, navigate to the project directory, and run the following command to install project dependencies:

composer install

4. Run Migrations: If your project uses a database, you may need to run migrations to set up the required tables. Run the following command to execute migrations:

php artisan migrate:refresh

5. Start the Local Development Server: To run the Laravel project on your local server, use the following command:

php artisan serve

This command will start the development server, and you should see a message indicating the server is running.

Access the Project: Open a web browser and enter the following URL:

http://127.0.0.1:8000


Screenshots : 
https://prnt.sc/rJneHbqLNTJ8
https://prnt.sc/5358SgX8-2x0